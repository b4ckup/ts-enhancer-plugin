//TSS_LOG = -logToFile true -file C:\Users\uli\Documents\Projects\ts-lang-plugin\log.txt -level warn
import * as ts from 'typescript/lib/tsserverlibrary';
import { Project, Node, Type, ObjectLiteralExpression, PropertyAssignment, CodeBlockWriter } from "ts-morph"
type CodeFixFn = (fileName: string, start: number, end: number, errorCodes: ReadonlyArray<number>, formatOptions: ts.FormatCodeSettings, preferences: ts.UserPreferences) => ReadonlyArray<ts.CodeFixAction>;
var old: CodeFixFn;
var service: ts.LanguageService;
var logger: ts.server.Logger;

interface test
{
  asd: boolean;
}

export = (mod: { typescript: typeof ts }) =>
    new Plugin(mod.typescript);
    
class Plugin
{
  constructor(private typescript: typeof ts)
  {
  }
  
  public create(info: ts.server.PluginCreateInfo): ts.LanguageService {
    old                            = info.languageService.getCodeFixesAtPosition;
    service                        = info.languageService;
    logger                         = info.project.projectService.logger; 
    service.getCodeFixesAtPosition = this.newFn;
    return info.languageService;
  }
  
  logNode(node: Node, indent: string = "")
  {
    let str = ts.SyntaxKind[node.getKind()];
    try
    {
      str += ": " + node.getType().getSymbol().getName();
    }
    catch
    {
    }
    console.log(indent + str)
    node.getChildren().forEach(c => this.logNode(c, indent + "  "));
  }
  
  findNode(node: Node, filter: (n: Node) => boolean): Node
  {
    if(filter(node) == true)
      return node;
      
    for(let n of node.getChildren())
    {
      let result = this.findNode(n, filter)
      if(result != null)
        return result;
    }
  }
  
  getLiteralObject(literal: ObjectLiteralExpression)
  {
    let literalSyntaxList = this.findNode(literal, n => n.getKind() == ts.SyntaxKind.SyntaxList);
    let res = {} as any;
    if(literalSyntaxList != null)
    {
      for(let prop of literalSyntaxList.getChildren().filter(c => c.getKind() == ts.SyntaxKind.PropertyAssignment))
      {
        let p = prop as PropertyAssignment;
        let val = prop.getChildren()[2];
        if(val != null)
        {
          if(val.getKind() == ts.SyntaxKind.ObjectLiteralExpression)
          {
            res[p.getName()] = this.getLiteralObject(val as ObjectLiteralExpression);
          }
          else //TODO: arrays
          {
            res[p.getName()] = val.getText();
          }
        }
      }
      return res;
    }
    return null;
  }
  
  getTypeObject(type: Type)
  {
    if(type.isAnonymous() == false && type.isInterface() == false)
      return null;
      
    let encounteredTypes: number[] = [
      (type.compilerType as any).id
    ];
    
    function construct(type: Type, encounteredTypes: number[])
    {
      let res = {} as any;
      for(let prop of type.getProperties())
      {
        let name = prop.getName();
        let decl = prop.getValueDeclaration();
        if(decl != null)
        {
          let t = decl.getType();
          if(t.isLiteral() == true)
          {
            res[name] = t.getText();
          }
          else if(t.getText() == "string")
          {
            res[name] = '""';
          }
          else if(t.isArray() == true)
          {
            res[name] = "[]";
          }
          else if(t.isAnonymous() == true || t.isInterface() == true)
          {
            let id = (t.compilerType as any).id;
            if(encounteredTypes.indexOf(id) < 0)
            {
              encounteredTypes.push(id);
              res[name] = construct(t, encounteredTypes);
            }
            else
            {
              res[name] = "null";
            }
          }
          else
          {
            res[name] = "null";
          }
        }
      }
      return res;
    }
    return construct(type, encounteredTypes);
  }
  
  getNewLiteralText(literal: ObjectLiteralExpression, type: Type)
  {
    let literalObj = this.getLiteralObject(literal);

    let typeObj = this.getTypeObject(type);
    if(literalObj == null || typeObj == null)
      return null;
    
    function merge(literal, type)
    {
      for(let prop in type)
      {
        if(!(prop in literal))
        {
          literal[prop] = type[prop];
        }
        else
        {
          if(typeof(literal[prop]) == "object" && typeof(type[prop]) == "object")
          {
            merge(literal[prop], type[prop]);
          }
        }
      }
    }
    
    function write(writer: CodeBlockWriter, obj, isLast: boolean = true)
    {
      if(typeof(obj) == "object")
      {
        writer.inlineBlock(() => {
          let ar = Object.keys(obj);
          for(let i=0; i<ar.length; i++)
          {
            let prop = ar[i];
            writer.write(`${prop}: `);
            if(i < ar.length - 1)
            {
              write(writer, obj[prop], false)
              writer.write(",\n");
            }
            else
            {
              write(writer, obj[prop])
            }
          }
        })
        if(isLast == false)
          writer.write(",");
      }
      else
      {
        writer.write(obj.toString());  
      }
    }
    
    merge(literalObj, typeObj);

    let writer = new CodeBlockWriter({
      indentNumberOfSpaces: 2,
    });
    write(writer, literalObj);
    let text = writer.toString();
    return text;
  }
  
  newFn: CodeFixFn = (fileName: string, start: number, end: number, errorCodes: ReadonlyArray<number>, formatOptions: ts.FormatCodeSettings, preferences: ts.UserPreferences) => 
  {
    let o = old(fileName, start, end, errorCodes, formatOptions, preferences);
    let action: ts.CodeFixAction = null;
    
    if(errorCodes.indexOf(2741) >= 0 || errorCodes.indexOf(2739) >= 0)
    {
      let p       = service.getProgram();
      let file    = p.getSourceFile(fileName);
      let project = new Project({
        compilerOptions: p.getCompilerOptions()
      });
      let text    = file.getText();
      let sf      = project.createSourceFile("dummy.ts", text);
      
      let node = this.findNode(sf, (n) => {
        let kind = n.getKind();
        return (kind == ts.SyntaxKind.ExpressionStatement || kind == ts.SyntaxKind.VariableDeclaration) && n.getStart() <= start && end <= n.getEnd()
      });
      
      let literal    = this.findNode(node, (n) => n.getKind() == ts.SyntaxKind.ObjectLiteralExpression);
      let identifier = this.findNode(node, (n) => n.getKind() == ts.SyntaxKind.Identifier);
      let type       = identifier.getType();
      if(type != null && literal != null && identifier != null && type.getSymbol() != null)
      {
        let text = this.getNewLiteralText(literal as ObjectLiteralExpression, type);
        action = {
          fixName: "fill undefined properties",
          description: "initialize undefined properties",
          changes: [
            {
              fileName,
              textChanges: [
                {
                  span: {
                    start: literal.getStart(),
                    length: literal.getEnd() - literal.getStart()
                  },
                  newText: text
                }
              ]
            }
          ]
        }
      }
    }
    if(action == null)
      return o;
    else
      return [action, ...o];
  }
}

