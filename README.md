# About
**ts-enhancer-plugin** is a simple plugin that I created to support some lacking features in the typescript language service. Luckily the service has a plugin api. Atm I only implemented an auto initialize feature for object literal that are supposed to be of interface or anonymous types. Let me know if theres any issues.  
You can debug the language server by setting the **TSS_LOG** environment variable to *-logToFile true -file somefile -level verbose*.  
If you're working in vscode you need to select the local typescript version: Execute command *TypeScript: Select TypeScript Version* and select *Use Workspace Version* (see [here](https://github.com/microsoft/TypeScript/wiki/Writing-a-Language-Service-Plugin) for more). You also obviously must have installed a local typescript version in your project.

# Installation
- execute `npm i --save-dev ts-enhancer-plugin`
- in **tsconfig.json** 
```json
{
  "compilerOptions": {
    "plugins": [
      { "name": "ts-enhancer-plugin" }
    ]
  }
}
```
- if you use vscode, make sure to have the local typescript version selected: Execute command *TypeScript: Select TypeScript Version* and select *Use Workspace Version* (see [here](https://github.com/microsoft/TypeScript/wiki/Writing-a-Language-Service-Plugin) for more).

# Showcase
![](https://gitlab.com/b4ckup/ts-enhancer-plugin/raw/8e389ce661c3c4f379b568ebb2674238bc2e5a27/img/example.gif)

# Thanks
thanks to [David Sherret](https://github.com/dsherret), the dev of [ts-morph](https://github.com/dsherret/ts-morph)